package br.gov.pa.sefa.siafen.gestaofinanceira.gateway.service.dto.empenho;

import java.io.Serializable;
import java.util.Date;

/**
 * A DTO for the Searches in Empenhos entity.
 */
/**
 * @author thiago
 *
 */
public class EmpenhoSearchDTO implements Serializable {

	private static final long serialVersionUID = -6496912512278584605L;

	private Long idUnidadeGestora;

	private Long idGestao;

	private Date searchStart;

	private Date searchStop;

	public EmpenhoSearchDTO() {
		super();
	}

	public Long getIdUnidadeGestora() {
		return idUnidadeGestora;
	}

	public void setIdUnidadeGestora(Long idUnidadeGestora) {
		this.idUnidadeGestora = idUnidadeGestora;
	}

	public Long getIdGestao() {
		return idGestao;
	}

	public void setIdGestao(Long idGestao) {
		this.idGestao = idGestao;
	}

	public Date getSearchStart() {
		return searchStart;
	}

	public void setSearchStart(Date searchStart) {
		this.searchStart = searchStart;
	}

	public Date getSearchStop() {
		return searchStop;
	}

	public void setSearchStop(Date searchStop) {
		this.searchStop = searchStop;
	}

	@Override
	public String toString() {
		return "EmpenhoSearchDTO [idUnidadeGestora=" + idUnidadeGestora + ", idGestao=" + idGestao + ", searchStart="
				+ searchStart + ", searchStop=" + searchStop + "]";
	}

}
