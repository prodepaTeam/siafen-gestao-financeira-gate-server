package br.gov.pa.sefa.siafen.gestaofinanceira.gateway.service.impl;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.gov.pa.sefa.siafen.gestaofinanceira.gateway.service.EmpenhoService;
import br.gov.pa.sefa.siafen.gestaofinanceira.gateway.service.dto.empenho.EmpenhoDetailDTO;
import br.gov.pa.sefa.siafen.gestaofinanceira.gateway.service.dto.empenho.EmpenhoItemListDTO;
import br.gov.pa.sefa.siafen.gestaofinanceira.gateway.service.dto.empenho.EmpenhoSearchDTO;

/**
 * Service Implementation for managing Empenho.
 */
@Service
@Transactional
public class EmpenhoServiceImpl implements EmpenhoService {

    private final Logger log = LoggerFactory.getLogger(EmpenhoServiceImpl.class);

    public EmpenhoServiceImpl() {
    }

    /**
     * Save a Empenho.
     *
     * @param EmpenhoDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public EmpenhoDetailDTO save(EmpenhoDetailDTO EmpenhoDTO) {
        log.debug("Request to save Empenho : {}", EmpenhoDTO);
        
        EmpenhoDetailDTO empenho = new EmpenhoDetailDTO();
        
        return empenho;
    }

    /**
     *  Get all the Empenhos.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<EmpenhoItemListDTO> findAll() {
        log.debug("Request to get all Empenhos");
        
        List<EmpenhoItemListDTO> list = new LinkedList<EmpenhoItemListDTO>();
        
        for (Long i = 0L; i < 10; i++) {
        	list.add(new EmpenhoItemListDTO(i, new Date(), "000"+i, ""+i, "unidadeGestora "+i, ""+i, "gestao "+i, i+".00"));
		}
        return list;
        
        /*return EmpenhoRepository.findAll().stream()
            .map(EmpenhoMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));*/
    }
    
    /**
     *  Get all the Empenhos.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<EmpenhoItemListDTO> search(EmpenhoSearchDTO search) {
        log.debug("Request to get all Empenhos");
        
        List<EmpenhoItemListDTO> list = new LinkedList<EmpenhoItemListDTO>();
        
        for (Long i = 0L; i < 15; i++) {
        	list.add(new EmpenhoItemListDTO(i, new Date(), "000"+i, ""+i, "unidadeGestora "+i, ""+i, "gestao "+i, i+".00"));
		}
        
        return list;
    }

    /**
     *  Get one Empenho by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public EmpenhoDetailDTO findOne(Long id) {
        log.debug("Request to get Empenho : {}", id);
        EmpenhoDetailDTO empenho = new EmpenhoDetailDTO();
        empenho.setId(id);
        empenho.setNumero(""+id);
        empenho.setIdUnidadeGestora(""+id);
        empenho.setUnidadeGestora("Unidade "+id);
        empenho.setIdGestao(""+id);
        empenho.setGestao("Gestao "+id);
        empenho.setValor("100"+id);
        empenho.setData(new Date());

        return empenho;
    }

    /**
     *  Delete the  Empenho by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Empenho : {}", id);
        //EmpenhoRepository.delete(id);
    }
}
