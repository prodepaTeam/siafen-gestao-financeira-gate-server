package br.gov.pa.sefa.siafen.gestaofinanceira.gateway.repository;

import br.gov.pa.sefa.siafen.gestaofinanceira.gateway.domain.Authority;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the Authority entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {
}
