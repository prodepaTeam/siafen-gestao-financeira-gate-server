package br.gov.pa.sefa.siafen.gestaofinanceira.gateway.service.mapper;

import br.gov.pa.sefa.siafen.gestaofinanceira.gateway.domain.*;
import br.gov.pa.sefa.siafen.gestaofinanceira.gateway.service.dto.DashboardItemDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity DashboardItem and its DTO DashboardItemDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DashboardItemMapper extends EntityMapper <DashboardItemDTO, DashboardItem> {
    
    
    default DashboardItem fromId(Long id) {
        if (id == null) {
            return null;
        }
        DashboardItem dashboardItem = new DashboardItem();
        dashboardItem.setId(id);
        return dashboardItem;
    }
}
