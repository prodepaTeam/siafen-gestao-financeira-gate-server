package br.gov.pa.sefa.siafen.gestaofinanceira.gateway.repository;

import br.gov.pa.sefa.siafen.gestaofinanceira.gateway.domain.DashboardItem;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the DashboardItem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DashboardItemRepository extends JpaRepository<DashboardItem,Long> {

}
