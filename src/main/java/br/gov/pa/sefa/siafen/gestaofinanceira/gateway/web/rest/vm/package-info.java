/**
 * View Models used by Spring MVC REST controllers.
 */
package br.gov.pa.sefa.siafen.gestaofinanceira.gateway.web.rest.vm;
