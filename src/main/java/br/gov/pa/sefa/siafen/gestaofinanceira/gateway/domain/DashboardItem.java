package br.gov.pa.sefa.siafen.gestaofinanceira.gateway.domain;

import io.swagger.annotations.ApiModel;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Itens Menu
 */
@ApiModel(description = "Itens Menu")
@Entity
@Table(name = "dashboard_item")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DashboardItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "short_name")
    private String shortName;

    @Column(name = "description")
    private String description;

    @Column(name = "jhi_order")
    private Integer order;

    @Column(name = "icon_name")
    private String iconName;

    @Column(name = "icon_color")
    private String iconColor;

    @Column(name = "background_color")
    private String backgroundColor;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public DashboardItem name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public DashboardItem shortName(String shortName) {
        this.shortName = shortName;
        return this;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getDescription() {
        return description;
    }

    public DashboardItem description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getOrder() {
        return order;
    }

    public DashboardItem order(Integer order) {
        this.order = order;
        return this;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public String getIconName() {
        return iconName;
    }

    public DashboardItem iconName(String iconName) {
        this.iconName = iconName;
        return this;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public String getIconColor() {
        return iconColor;
    }

    public DashboardItem iconColor(String iconColor) {
        this.iconColor = iconColor;
        return this;
    }

    public void setIconColor(String iconColor) {
        this.iconColor = iconColor;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public DashboardItem backgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
        return this;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DashboardItem dashboardItem = (DashboardItem) o;
        if (dashboardItem.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dashboardItem.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DashboardItem{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", shortName='" + getShortName() + "'" +
            ", description='" + getDescription() + "'" +
            ", order='" + getOrder() + "'" +
            ", iconName='" + getIconName() + "'" +
            ", iconColor='" + getIconColor() + "'" +
            ", backgroundColor='" + getBackgroundColor() + "'" +
            "}";
    }
}
