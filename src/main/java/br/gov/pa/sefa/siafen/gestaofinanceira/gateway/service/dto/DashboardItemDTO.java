package br.gov.pa.sefa.siafen.gestaofinanceira.gateway.service.dto;


import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the DashboardItem entity.
 */
public class DashboardItemDTO implements Serializable {

    private Long id;

    private String name;

    private String shortName;

    private String description;

    private Integer order;

    private String iconName;

    private String iconColor;

    private String backgroundColor;

    
    
    public DashboardItemDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
       

	public DashboardItemDTO(Long id, String name, String shortName, String description, Integer order, String iconName,
			String iconColor, String backgroundColor) {
		super();
		this.id = id;
		this.name = name;
		this.shortName = shortName;
		this.description = description;
		this.order = order;
		this.iconName = iconName;
		this.iconColor = iconColor;
		this.backgroundColor = backgroundColor;
	}



	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public String getIconColor() {
        return iconColor;
    }

    public void setIconColor(String iconColor) {
        this.iconColor = iconColor;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DashboardItemDTO dashboardItemDTO = (DashboardItemDTO) o;
        if(dashboardItemDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dashboardItemDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DashboardItemDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", shortName='" + getShortName() + "'" +
            ", description='" + getDescription() + "'" +
            ", order='" + getOrder() + "'" +
            ", iconName='" + getIconName() + "'" +
            ", iconColor='" + getIconColor() + "'" +
            ", backgroundColor='" + getBackgroundColor() + "'" +
            "}";
    }
}
