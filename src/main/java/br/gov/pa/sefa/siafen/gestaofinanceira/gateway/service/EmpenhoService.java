package br.gov.pa.sefa.siafen.gestaofinanceira.gateway.service;

import java.util.List;

import br.gov.pa.sefa.siafen.gestaofinanceira.gateway.service.dto.empenho.EmpenhoDetailDTO;
import br.gov.pa.sefa.siafen.gestaofinanceira.gateway.service.dto.empenho.EmpenhoItemListDTO;
import br.gov.pa.sefa.siafen.gestaofinanceira.gateway.service.dto.empenho.EmpenhoSearchDTO;

/**
 * Service Interface for managing Empenho.
 */
public interface EmpenhoService {

    /**
     * Save a Empenho.
     *
     * @param EmpenhoDTO the entity to save
     * @return the persisted entity
     */
    EmpenhoDetailDTO save(EmpenhoDetailDTO EmpenhoDTO);

    /**
     *  Get all the Empenhos.
     *
     *  @return the list of entities
     */
    List<EmpenhoItemListDTO> findAll();
    
    /**
     *  Get all the Empenhos.
     *
     *  @return the list of entities
     */
    List<EmpenhoItemListDTO> search(EmpenhoSearchDTO search);

    /**
     *  Get the "id" Empenho.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    EmpenhoDetailDTO findOne(Long id);

    /**
     *  Delete the "id" Empenho.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
