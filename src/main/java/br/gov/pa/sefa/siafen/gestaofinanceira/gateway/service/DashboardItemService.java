package br.gov.pa.sefa.siafen.gestaofinanceira.gateway.service;

import br.gov.pa.sefa.siafen.gestaofinanceira.gateway.service.dto.DashboardItemDTO;
import java.util.List;

/**
 * Service Interface for managing DashboardItem.
 */
public interface DashboardItemService {

    /**
     * Save a dashboardItem.
     *
     * @param dashboardItemDTO the entity to save
     * @return the persisted entity
     */
    DashboardItemDTO save(DashboardItemDTO dashboardItemDTO);

    /**
     *  Get all the dashboardItems.
     *
     *  @return the list of entities
     */
    List<DashboardItemDTO> findAll();

    /**
     *  Get the "id" dashboardItem.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    DashboardItemDTO findOne(Long id);

    /**
     *  Delete the "id" dashboardItem.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
