package br.gov.pa.sefa.siafen.gestaofinanceira.gateway.web.rest;

import com.codahale.metrics.annotation.Timed;
import br.gov.pa.sefa.siafen.gestaofinanceira.gateway.service.DashboardItemService;
import br.gov.pa.sefa.siafen.gestaofinanceira.gateway.web.rest.util.HeaderUtil;
import br.gov.pa.sefa.siafen.gestaofinanceira.gateway.service.dto.DashboardItemDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing DashboardItem.
 */
@RestController
@RequestMapping("/api")
public class DashboardItemResource {

    private final Logger log = LoggerFactory.getLogger(DashboardItemResource.class);

    private static final String ENTITY_NAME = "dashboardItem";

    private final DashboardItemService dashboardItemService;

    public DashboardItemResource(DashboardItemService dashboardItemService) {
        this.dashboardItemService = dashboardItemService;
    }

    /**
     * POST  /dashboard-items : Create a new dashboardItem.
     *
     * @param dashboardItemDTO the dashboardItemDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new dashboardItemDTO, or with status 400 (Bad Request) if the dashboardItem has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/dashboard-items")
    @Timed
    public ResponseEntity<DashboardItemDTO> createDashboardItem(@RequestBody DashboardItemDTO dashboardItemDTO) throws URISyntaxException {
        log.debug("REST request to save DashboardItem : {}", dashboardItemDTO);
        if (dashboardItemDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new dashboardItem cannot already have an ID")).body(null);
        }
        DashboardItemDTO result = dashboardItemService.save(dashboardItemDTO);
        return ResponseEntity.created(new URI("/api/dashboard-items/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /dashboard-items : Updates an existing dashboardItem.
     *
     * @param dashboardItemDTO the dashboardItemDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated dashboardItemDTO,
     * or with status 400 (Bad Request) if the dashboardItemDTO is not valid,
     * or with status 500 (Internal Server Error) if the dashboardItemDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/dashboard-items")
    @Timed
    public ResponseEntity<DashboardItemDTO> updateDashboardItem(@RequestBody DashboardItemDTO dashboardItemDTO) throws URISyntaxException {
        log.debug("REST request to update DashboardItem : {}", dashboardItemDTO);
        if (dashboardItemDTO.getId() == null) {
            return createDashboardItem(dashboardItemDTO);
        }
        DashboardItemDTO result = dashboardItemService.save(dashboardItemDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, dashboardItemDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /dashboard-items : get all the dashboardItems.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of dashboardItems in body
     */
    @GetMapping("/dashboard-items")
    @Timed
    public List<DashboardItemDTO> getAllDashboardItems() {
        log.debug("REST request to get all DashboardItems");
        return dashboardItemService.findAll();
    }

    /**
     * GET  /dashboard-items/:id : get the "id" dashboardItem.
     *
     * @param id the id of the dashboardItemDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dashboardItemDTO, or with status 404 (Not Found)
     */
    @GetMapping("/dashboard-items/{id}")
    @Timed
    public ResponseEntity<DashboardItemDTO> getDashboardItem(@PathVariable Long id) {
        log.debug("REST request to get DashboardItem : {}", id);
        DashboardItemDTO dashboardItemDTO = dashboardItemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dashboardItemDTO));
    }

    /**
     * DELETE  /dashboard-items/:id : delete the "id" dashboardItem.
     *
     * @param id the id of the dashboardItemDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/dashboard-items/{id}")
    @Timed
    public ResponseEntity<Void> deleteDashboardItem(@PathVariable Long id) {
        log.debug("REST request to delete DashboardItem : {}", id);
        dashboardItemService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
