package br.gov.pa.sefa.siafen.gestaofinanceira.gateway.web.rest;

import com.codahale.metrics.annotation.Timed;
import br.gov.pa.sefa.siafen.gestaofinanceira.gateway.service.EmpenhoService;
import br.gov.pa.sefa.siafen.gestaofinanceira.gateway.service.dto.empenho.EmpenhoDetailDTO;
import br.gov.pa.sefa.siafen.gestaofinanceira.gateway.service.dto.empenho.EmpenhoItemListDTO;
import br.gov.pa.sefa.siafen.gestaofinanceira.gateway.service.dto.empenho.EmpenhoSearchDTO;
import br.gov.pa.sefa.siafen.gestaofinanceira.gateway.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Empenho.
 */
@RestController
@RequestMapping("/api")
public class EmpenhoResource {

    private final Logger log = LoggerFactory.getLogger(EmpenhoResource.class);

    private static final String ENTITY_NAME = "empenho";

    private final EmpenhoService empenhoService;

    public EmpenhoResource(EmpenhoService empenhoService) {
        this.empenhoService = empenhoService;
    }

    /**
     * POST  /dashboard-items : Create a new empenho.
     *
     * @param empenhoDTO the empenhoDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new empenhoDTO, or with status 400 (Bad Request) if the empenho has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/empenhos")
    @Timed
    public ResponseEntity<EmpenhoDetailDTO> createEmpenho(@RequestBody EmpenhoDetailDTO empenhoDTO) throws URISyntaxException {
        log.debug("REST request to save Empenho : {}", empenhoDTO);
        if (empenhoDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new empenho cannot already have an ID")).body(null);
        }
        EmpenhoDetailDTO result = empenhoService.save(empenhoDTO);
        return ResponseEntity.created(new URI("/api/dashboard-items/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /dashboard-items : Updates an existing empenho.
     *
     * @param empenhoDTO the empenhoDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated empenhoDTO,
     * or with status 400 (Bad Request) if the empenhoDTO is not valid,
     * or with status 500 (Internal Server Error) if the empenhoDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/empenhos")
    @Timed
    public ResponseEntity<EmpenhoDetailDTO> updateEmpenho(@RequestBody EmpenhoDetailDTO empenhoDTO) throws URISyntaxException {
        log.debug("REST request to update Empenho : {}", empenhoDTO);
        if (empenhoDTO.getId() == null) {
            return createEmpenho(empenhoDTO);
        }
        EmpenhoDetailDTO result = empenhoService.save(empenhoDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, empenhoDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /dashboard-items : get all the empenhos.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of empenhos in body
     */
    @GetMapping("/empenhos")
    @Timed
    public List<EmpenhoItemListDTO> getAllEmpenhos() {
        log.debug("REST request to get all Empenhos");
        return empenhoService.findAll();
    }
    
    /**
     * GET  /dashboard-items : get all the empenhos.
     * @param search 
     *
     * @return the ResponseEntity with status 200 (OK) and the list of empenhos in body
     */
    @GetMapping("/empenhos/search")
    @Timed
    public List<EmpenhoItemListDTO> getAllEmpenhosSearch(@RequestBody EmpenhoSearchDTO search) {
        log.debug("REST request to get all Empenhos");
        return empenhoService.search(search);
    }

    /**
     * GET  /dashboard-items/:id : get the "id" empenho.
     *
     * @param id the id of the empenhoDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the empenhoDTO, or with status 404 (Not Found)
     */
    @GetMapping("/empenhos/{id}")
    @Timed
    public ResponseEntity<EmpenhoDetailDTO> getEmpenho(@PathVariable Long id) {
        log.debug("REST request to get Empenho : {}", id);
        EmpenhoDetailDTO empenhoDTO = empenhoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(empenhoDTO));
    }

    /**
     * DELETE  /dashboard-items/:id : delete the "id" empenho.
     *
     * @param id the id of the empenhoDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/empenhos/{id}")
    @Timed
    public ResponseEntity<Void> deleteEmpenho(@PathVariable Long id) {
        log.debug("REST request to delete Empenho : {}", id);
        empenhoService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
