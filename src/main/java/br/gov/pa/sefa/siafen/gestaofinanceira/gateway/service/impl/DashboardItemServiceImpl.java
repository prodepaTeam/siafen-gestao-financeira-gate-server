package br.gov.pa.sefa.siafen.gestaofinanceira.gateway.service.impl;

import br.gov.pa.sefa.siafen.gestaofinanceira.gateway.service.DashboardItemService;
import br.gov.pa.sefa.siafen.gestaofinanceira.gateway.domain.DashboardItem;
import br.gov.pa.sefa.siafen.gestaofinanceira.gateway.repository.DashboardItemRepository;
import br.gov.pa.sefa.siafen.gestaofinanceira.gateway.service.dto.DashboardItemDTO;
import br.gov.pa.sefa.siafen.gestaofinanceira.gateway.service.mapper.DashboardItemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing DashboardItem.
 */
@Service
@Transactional
public class DashboardItemServiceImpl implements DashboardItemService{

    private final Logger log = LoggerFactory.getLogger(DashboardItemServiceImpl.class);

    private final DashboardItemRepository dashboardItemRepository;

    private final DashboardItemMapper dashboardItemMapper;

    public DashboardItemServiceImpl(DashboardItemRepository dashboardItemRepository, DashboardItemMapper dashboardItemMapper) {
        this.dashboardItemRepository = dashboardItemRepository;
        this.dashboardItemMapper = dashboardItemMapper;
    }

    /**
     * Save a dashboardItem.
     *
     * @param dashboardItemDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public DashboardItemDTO save(DashboardItemDTO dashboardItemDTO) {
        log.debug("Request to save DashboardItem : {}", dashboardItemDTO);
        DashboardItem dashboardItem = dashboardItemMapper.toEntity(dashboardItemDTO);
        dashboardItem = dashboardItemRepository.save(dashboardItem);
        return dashboardItemMapper.toDto(dashboardItem);
    }

    /**
     *  Get all the dashboardItems.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<DashboardItemDTO> findAll() {
        log.debug("Request to get all DashboardItems");
        
        /*List<DashboardItemDTO> list = new LinkedList();
        
        list.add(new DashboardItemDTO(1L, "Empenhos de Notas", "Empenho", "Módulo de Empenhos", 1, "fa fa-university", "bg-green", "#f7f7f7"));
        list.add(new DashboardItemDTO(2L, "Consultas de Empenhos", "Consulas", "Módulo de Empenhos", 2, "fa fa-handshake-o", "bg-gray-light", "#f7f7f7"));
        list.add(new DashboardItemDTO(3L, "Cancelamento de Empenhos", "Cancelamento", "Módulo de Empenhos", 3, "fa fa-times", "bg-green", "#f7f7f7"));
        list.add(new DashboardItemDTO(4L, "Consultas Gerênciais", "Consultas Ger", "Módulo de Empenhos", 4, "fa fa-search-plus", "bg-yellow", "#f7f7f7"));
        list.add(new DashboardItemDTO(5L, "Catálogo de Notas", "Catálogo", "Módulo de Empenhos", 5, "fa fa-id-card-o", "bg-green", "#f7f7f7"));
        
        return list;*/
        
        return dashboardItemRepository.findAll().stream()
            .map(dashboardItemMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
        
    }

    /**
     *  Get one dashboardItem by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public DashboardItemDTO findOne(Long id) {
        log.debug("Request to get DashboardItem : {}", id);
        DashboardItem dashboardItem = dashboardItemRepository.findOne(id);
        return dashboardItemMapper.toDto(dashboardItem);
    }

    /**
     *  Delete the  dashboardItem by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete DashboardItem : {}", id);
        dashboardItemRepository.delete(id);
    }
}
