package br.gov.pa.sefa.siafen.gestaofinanceira.gateway.service.dto.empenho;

import java.io.Serializable;
import java.util.Date;

/**
 * A DTO for the DashboardItem entity.
 */
public class EmpenhoItemListDTO implements Serializable {

	private static final long serialVersionUID = 7572039040517920148L;

	private Long id;

	private Date data;

	private String numero;

	private String idUnidadeGestora;

	private String unidadeGestora;

	private String idGestao;

	private String gestao;

	private String valor;

	public EmpenhoItemListDTO() {
		super();
	}

	public EmpenhoItemListDTO(Long id, Date data, String numero, String idUnidadeGestora, String unidadeGestora,
			String idGestao, String gestao, String valor) {
		super();
		this.id = id;
		this.data = data;
		this.numero = numero;
		this.idUnidadeGestora = idUnidadeGestora;
		this.unidadeGestora = unidadeGestora;
		this.idGestao = idGestao;
		this.gestao = gestao;
		this.valor = valor;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getIdUnidadeGestora() {
		return idUnidadeGestora;
	}

	public void setIdUnidadeGestora(String idUnidadeGestora) {
		this.idUnidadeGestora = idUnidadeGestora;
	}

	public String getUnidadeGestora() {
		return unidadeGestora;
	}

	public void setUnidadeGestora(String unidadeGestora) {
		this.unidadeGestora = unidadeGestora;
	}

	public String getIdGestao() {
		return idGestao;
	}

	public void setIdGestao(String idGestao) {
		this.idGestao = idGestao;
	}

	public String getGestao() {
		return gestao;
	}

	public void setGestao(String gestao) {
		this.gestao = gestao;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	@Override
	public String toString() {
		return "EmpenhoItemListDTO [id=" + id + ", data=" + data + ", numero=" + numero + ", idUnidadeGestora="
				+ idUnidadeGestora + ", unidadeGestora=" + unidadeGestora + ", idGestao=" + idGestao + ", gestao="
				+ gestao + ", valor=" + valor + "]";
	}
	
}