package br.gov.pa.sefa.siafen.gestaofinanceira.gateway.web.rest;

import br.gov.pa.sefa.siafen.gestaofinanceira.gateway.GestaoFinanceiraApp;

import br.gov.pa.sefa.siafen.gestaofinanceira.gateway.domain.DashboardItem;
import br.gov.pa.sefa.siafen.gestaofinanceira.gateway.repository.DashboardItemRepository;
import br.gov.pa.sefa.siafen.gestaofinanceira.gateway.service.DashboardItemService;
import br.gov.pa.sefa.siafen.gestaofinanceira.gateway.service.dto.DashboardItemDTO;
import br.gov.pa.sefa.siafen.gestaofinanceira.gateway.service.mapper.DashboardItemMapper;
import br.gov.pa.sefa.siafen.gestaofinanceira.gateway.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DashboardItemResource REST controller.
 *
 * @see DashboardItemResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GestaoFinanceiraApp.class)
public class DashboardItemResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_SHORT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_SHORT_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Integer DEFAULT_ORDER = 1;
    private static final Integer UPDATED_ORDER = 2;

    private static final String DEFAULT_ICON_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ICON_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ICON_COLOR = "AAAAAAAAAA";
    private static final String UPDATED_ICON_COLOR = "BBBBBBBBBB";

    private static final String DEFAULT_BACKGROUND_COLOR = "AAAAAAAAAA";
    private static final String UPDATED_BACKGROUND_COLOR = "BBBBBBBBBB";

    @Autowired
    private DashboardItemRepository dashboardItemRepository;

    @Autowired
    private DashboardItemMapper dashboardItemMapper;

    @Autowired
    private DashboardItemService dashboardItemService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDashboardItemMockMvc;

    private DashboardItem dashboardItem;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        DashboardItemResource dashboardItemResource = new DashboardItemResource(dashboardItemService);
        this.restDashboardItemMockMvc = MockMvcBuilders.standaloneSetup(dashboardItemResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DashboardItem createEntity(EntityManager em) {
        DashboardItem dashboardItem = new DashboardItem()
            .name(DEFAULT_NAME)
            .shortName(DEFAULT_SHORT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .order(DEFAULT_ORDER)
            .iconName(DEFAULT_ICON_NAME)
            .iconColor(DEFAULT_ICON_COLOR)
            .backgroundColor(DEFAULT_BACKGROUND_COLOR);
        return dashboardItem;
    }

    @Before
    public void initTest() {
        dashboardItem = createEntity(em);
    }

    @Test
    @Transactional
    public void createDashboardItem() throws Exception {
        int databaseSizeBeforeCreate = dashboardItemRepository.findAll().size();

        // Create the DashboardItem
        DashboardItemDTO dashboardItemDTO = dashboardItemMapper.toDto(dashboardItem);
        restDashboardItemMockMvc.perform(post("/api/dashboard-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dashboardItemDTO)))
            .andExpect(status().isCreated());

        // Validate the DashboardItem in the database
        List<DashboardItem> dashboardItemList = dashboardItemRepository.findAll();
        assertThat(dashboardItemList).hasSize(databaseSizeBeforeCreate + 1);
        DashboardItem testDashboardItem = dashboardItemList.get(dashboardItemList.size() - 1);
        assertThat(testDashboardItem.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testDashboardItem.getShortName()).isEqualTo(DEFAULT_SHORT_NAME);
        assertThat(testDashboardItem.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testDashboardItem.getOrder()).isEqualTo(DEFAULT_ORDER);
        assertThat(testDashboardItem.getIconName()).isEqualTo(DEFAULT_ICON_NAME);
        assertThat(testDashboardItem.getIconColor()).isEqualTo(DEFAULT_ICON_COLOR);
        assertThat(testDashboardItem.getBackgroundColor()).isEqualTo(DEFAULT_BACKGROUND_COLOR);
    }

    @Test
    @Transactional
    public void createDashboardItemWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dashboardItemRepository.findAll().size();

        // Create the DashboardItem with an existing ID
        dashboardItem.setId(1L);
        DashboardItemDTO dashboardItemDTO = dashboardItemMapper.toDto(dashboardItem);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDashboardItemMockMvc.perform(post("/api/dashboard-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dashboardItemDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<DashboardItem> dashboardItemList = dashboardItemRepository.findAll();
        assertThat(dashboardItemList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllDashboardItems() throws Exception {
        // Initialize the database
        dashboardItemRepository.saveAndFlush(dashboardItem);

        // Get all the dashboardItemList
        restDashboardItemMockMvc.perform(get("/api/dashboard-items?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dashboardItem.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].shortName").value(hasItem(DEFAULT_SHORT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].order").value(hasItem(DEFAULT_ORDER)))
            .andExpect(jsonPath("$.[*].iconName").value(hasItem(DEFAULT_ICON_NAME.toString())))
            .andExpect(jsonPath("$.[*].iconColor").value(hasItem(DEFAULT_ICON_COLOR.toString())))
            .andExpect(jsonPath("$.[*].backgroundColor").value(hasItem(DEFAULT_BACKGROUND_COLOR.toString())));
    }

    @Test
    @Transactional
    public void getDashboardItem() throws Exception {
        // Initialize the database
        dashboardItemRepository.saveAndFlush(dashboardItem);

        // Get the dashboardItem
        restDashboardItemMockMvc.perform(get("/api/dashboard-items/{id}", dashboardItem.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dashboardItem.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.shortName").value(DEFAULT_SHORT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.order").value(DEFAULT_ORDER))
            .andExpect(jsonPath("$.iconName").value(DEFAULT_ICON_NAME.toString()))
            .andExpect(jsonPath("$.iconColor").value(DEFAULT_ICON_COLOR.toString()))
            .andExpect(jsonPath("$.backgroundColor").value(DEFAULT_BACKGROUND_COLOR.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDashboardItem() throws Exception {
        // Get the dashboardItem
        restDashboardItemMockMvc.perform(get("/api/dashboard-items/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDashboardItem() throws Exception {
        // Initialize the database
        dashboardItemRepository.saveAndFlush(dashboardItem);
        int databaseSizeBeforeUpdate = dashboardItemRepository.findAll().size();

        // Update the dashboardItem
        DashboardItem updatedDashboardItem = dashboardItemRepository.findOne(dashboardItem.getId());
        updatedDashboardItem
            .name(UPDATED_NAME)
            .shortName(UPDATED_SHORT_NAME)
            .description(UPDATED_DESCRIPTION)
            .order(UPDATED_ORDER)
            .iconName(UPDATED_ICON_NAME)
            .iconColor(UPDATED_ICON_COLOR)
            .backgroundColor(UPDATED_BACKGROUND_COLOR);
        DashboardItemDTO dashboardItemDTO = dashboardItemMapper.toDto(updatedDashboardItem);

        restDashboardItemMockMvc.perform(put("/api/dashboard-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dashboardItemDTO)))
            .andExpect(status().isOk());

        // Validate the DashboardItem in the database
        List<DashboardItem> dashboardItemList = dashboardItemRepository.findAll();
        assertThat(dashboardItemList).hasSize(databaseSizeBeforeUpdate);
        DashboardItem testDashboardItem = dashboardItemList.get(dashboardItemList.size() - 1);
        assertThat(testDashboardItem.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testDashboardItem.getShortName()).isEqualTo(UPDATED_SHORT_NAME);
        assertThat(testDashboardItem.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testDashboardItem.getOrder()).isEqualTo(UPDATED_ORDER);
        assertThat(testDashboardItem.getIconName()).isEqualTo(UPDATED_ICON_NAME);
        assertThat(testDashboardItem.getIconColor()).isEqualTo(UPDATED_ICON_COLOR);
        assertThat(testDashboardItem.getBackgroundColor()).isEqualTo(UPDATED_BACKGROUND_COLOR);
    }

    @Test
    @Transactional
    public void updateNonExistingDashboardItem() throws Exception {
        int databaseSizeBeforeUpdate = dashboardItemRepository.findAll().size();

        // Create the DashboardItem
        DashboardItemDTO dashboardItemDTO = dashboardItemMapper.toDto(dashboardItem);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDashboardItemMockMvc.perform(put("/api/dashboard-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dashboardItemDTO)))
            .andExpect(status().isCreated());

        // Validate the DashboardItem in the database
        List<DashboardItem> dashboardItemList = dashboardItemRepository.findAll();
        assertThat(dashboardItemList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteDashboardItem() throws Exception {
        // Initialize the database
        dashboardItemRepository.saveAndFlush(dashboardItem);
        int databaseSizeBeforeDelete = dashboardItemRepository.findAll().size();

        // Get the dashboardItem
        restDashboardItemMockMvc.perform(delete("/api/dashboard-items/{id}", dashboardItem.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<DashboardItem> dashboardItemList = dashboardItemRepository.findAll();
        assertThat(dashboardItemList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DashboardItem.class);
        DashboardItem dashboardItem1 = new DashboardItem();
        dashboardItem1.setId(1L);
        DashboardItem dashboardItem2 = new DashboardItem();
        dashboardItem2.setId(dashboardItem1.getId());
        assertThat(dashboardItem1).isEqualTo(dashboardItem2);
        dashboardItem2.setId(2L);
        assertThat(dashboardItem1).isNotEqualTo(dashboardItem2);
        dashboardItem1.setId(null);
        assertThat(dashboardItem1).isNotEqualTo(dashboardItem2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DashboardItemDTO.class);
        DashboardItemDTO dashboardItemDTO1 = new DashboardItemDTO();
        dashboardItemDTO1.setId(1L);
        DashboardItemDTO dashboardItemDTO2 = new DashboardItemDTO();
        assertThat(dashboardItemDTO1).isNotEqualTo(dashboardItemDTO2);
        dashboardItemDTO2.setId(dashboardItemDTO1.getId());
        assertThat(dashboardItemDTO1).isEqualTo(dashboardItemDTO2);
        dashboardItemDTO2.setId(2L);
        assertThat(dashboardItemDTO1).isNotEqualTo(dashboardItemDTO2);
        dashboardItemDTO1.setId(null);
        assertThat(dashboardItemDTO1).isNotEqualTo(dashboardItemDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(dashboardItemMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(dashboardItemMapper.fromId(null)).isNull();
    }
}
