package br.gov.pa.sefa.siafen.gestaofinanceira.gateway.cucumber.stepdefs;

import br.gov.pa.sefa.siafen.gestaofinanceira.gateway.GestaoFinanceiraApp;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import org.springframework.boot.test.context.SpringBootTest;

@WebAppConfiguration
@SpringBootTest
@ContextConfiguration(classes = GestaoFinanceiraApp.class)
public abstract class StepDefs {

    protected ResultActions actions;

}
